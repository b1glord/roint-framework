//
//  GRF.h
//  ROInt
//
//  Created by Luis Sergio Saboia Moura Junior on 4/12/11.
//  Copyright 2011 IceTempest. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ROGRFFile;
@class TreeNode;

@interface ROGRF : NSObject {
    struct ROGrf *grf;
    TreeNode *root;
    
    int filecount;
}

@property(nonatomic, readonly) int filecount;

-(bool)load:(NSString*)filename;
-(void)close;

-(ROGRFFile*)fileAtIndex:(unsigned int)idx;
-(ROGRFFile*)fileWithName:(const char*)fn;

@end
