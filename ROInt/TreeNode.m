//
//  TreeNode.m
//  ROInt
//
//  Created by Luis Sergio Saboia Moura Junior on 4/12/11.
//  Copyright 2011 IceTempest. All rights reserved.
//

#import "TreeNode.h"


@implementation TreeNode

@synthesize idx;
@synthesize right, left;

- (id)init {
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)dealloc {
    [right release];
    [left release];
    
    [super dealloc];
}

@end
