Open-Ragnarok

To fully use this framework, you need to retrieve the needed submodule. Do this by issuing the following commands on the git repository:

> git submodule init
> git submodule update
